<?php
/**
 * Copyright (c) 2011, 2015, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$App->Promotion = TRUE;

// Define your project-wide Nav bars here.
$Nav = new Nav();
$Nav->addNavSeparator("Security", 'index.php');
$Nav->addCustomNav("Mail the Security Team", "mailto:security@eclipse-foundation.org", "_self", 2);
$Nav->addCustomNav("Team Members", "team.php", "_self", 2);
$Nav->addCustomNav("Policy", "policy.php", "_self", 2);
$Nav->addCustomNav("Known Vulnerabilities", "known.php", "_self", 2);
$Nav->addNavSeparator("Projects", "/projects");
$Nav->addCustomNav("List of Projects", "/projects/listofprojects.php", "_self", 2);
$Nav->addCustomNav("Project Tools", "/projects/tools", "_self", 2);
$Theme->setNav($Nav);