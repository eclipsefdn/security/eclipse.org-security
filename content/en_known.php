<?php
/**
 * Copyright (c) 2011, 2015, 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/debug.php");
trace_file_info(__FILE__);

require_once($_SERVER['DOCUMENT_ROOT'] . "/projects/classes/Bug.class.php");
$bugs = findResolvedSecurityBugs();
?>
<?php
  print "<ul>";
  foreach ($bugs as $bug) {
      $line = "<a href=\"{$bug->getLink()}\">{$bug->getId()}</a>  {$bug->getProduct()}: {$bug->getTitle()}";
      if ($cve = $bug->getCVE()) {
          $link = "<a href=\"https://cve.mitre.org/cgi-bin/cvename.cgi?name=$cve\">$cve</a>";
          $line .= " ($link)";
      }
    print "<li>" . $line . "</li>";
  }
  print "</ul>";