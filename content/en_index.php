<?php
/**
 * Copyright (c) 2011, 2015, 2018, 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<h2>How to report a vulnerability?</h2>
<p>
  If you would like to report a security vulnerability in an Eclipse
  Foundation project, first check the project's repository for the
  <tt>SECURITY.md</tt> file and follow specific instructions for that
  project. If there is no specific information there, you have two
  options. Either report the issue by email to the
  <a href="mailto:security@eclipse-foundation.org">Eclipse Foundation Security Team</a>,
  or use the <a href="https://gitlab.eclipse.org/security/vulnerability-reports/-/issues/new?issuable_template=new_vulnerability">dedicated issue tracker</a>.
</p>

<h2>Additional information</h2>

<p>The <strong>Eclipse Foundation Security Team</strong> provides help and
  advice to Eclipse Foundation
  projects on vulnerability issues and is the first point of contact for
  handling security vulnerabilities. Members of the Eclipse Foundation
  Security Team are selected amongs committers on Eclipse Projects, members
  of the Eclipse Architecture Council, and Eclipse Foundation staff.
</p>

</p>
  The general security mailing list address is <a
    href="mailto:security@eclipse-foundation.org">security@eclipse-foundation.org</a>. Members
  of the Eclipse Foundation Security Team will receive messages sent to this
  address. This address should be used only for reporting undisclosed
  vulnerabilities; regular issue reports and questions unrelated to
  vulnerabilities in Eclipse Foundation software will be ignored. Note that
  this email set to this address is not encrypted.
</p>
<p><strong>Note that, as a matter of policy, the security team does not
	open attachments.</strong></p>
<p>
  The community is also encouraged to report vulnerabilities using the
  <a href="https://gitlab.eclipse.org/security/vulnerability-reports/-/issues/new?issuable_template=new_vulnerability">
  Eclipse Foundation's issue tracker</a>. Note that you will
  need an Eclipse Foundation account to create an issue report
  (<a href="https://accounts.eclipse.org/user/register?destination=user">create
  an account here if you do not have one</a>), but by doing so you will be able to participate
  directly in the resolution of the issue.
</p>
<p>
  Issue reports related to vulnerabilities must be marked as
  &quot;confidential&quot;, either automatically by clicking the provided
  link by the reporter, or by a committer during the triage process.
</p>
<h2>Disclosure</h2>
<p>
  The timing and manner of disclosure is governed by
  the <a href="policy.php">Eclipse Foundation Vulnerability Reporting Policy</a>.
</p>
<p>
  Publicly disclosed issues are listed on the <a href="known.php">Disclosed
  Vulnerabilities Page</a>.
</p>
